const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();

module.exports.hello = async (event, context) => {
    const srcBucket = event.Records[0].s3.bucket.name;
    const srcKey = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, " "));
    const params = {Bucket: srcBucket, Key: srcKey};
    try {
        console.log('Start!!');
        const data = await s3.getObject(params).promise();
        const files = await imagemin.buffer(data.Body,{
            plugins: [
                imageminJpegtran(),
                imageminPngquant({quality: '65-80'})
            ],
            use:[imageminJpegtran(), imageminPngquant({quality: '70-80'})]
        });
        return {
            statusCode: 200,
            body: 'Success!!'
        }
    }
    catch (err) {
        return {
          statusCode: err.statusCode || 400,
          body: err.message || JSON.stringify(err.message)
        }
    }
};