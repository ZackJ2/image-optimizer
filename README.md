#Based on Serverless framework(https://serverless.com/) to develop.
#First you need to set up s3 image event to triger this lambda function on AWS platform.

##Deploy the Service

```
$ serverless deploy -v
```

##Deploy the Function

```
$ serverless deploy function -f hello
```

##Invoke the Function

```
$ serverless invoke -f hello -l
```

##Fetch the Function Logs

```
$ serverless logs -f hello -t
```

